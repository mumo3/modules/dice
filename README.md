# Dice

This is a module for mumo.

It allow rolling dice. It also display a summary if you roll too much dice at once.


## Usage

!r XdY + Z for rolling X dice Y plus Z

!rp XdY + Z  for rolling X dice Y plus Z, the result is only displayed to you
 
## Example

Output example for 6 dices with 6 faces:

![](example.png)

## Client blacklist

Since some client can't render HTML properly (or at all), dice send different result to each user in the channel according to thier client :
- Only dices results and total : iOS, Android, No os, Browser
- Dices results and recapitulative table : everything else
