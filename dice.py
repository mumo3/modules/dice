#!/usr/bin/env python
# -*- coding: utf-8

from mumo_module import (commaSeperatedIntegers,
                         commaSeperatedBool,
                         commaSeperatedStrings,
                         MumoModule)
import concurrent.futures
import json
import pymumble_py3
import re
import subprocess as sp
import time
import wave
import youtube_dl
import random

class dice(MumoModule):
    default_config = {'dice':(
                                ('servers', commaSeperatedIntegers, []),
                                ),
                    }
    def __init__(self, name, manager, configuration = None):
        MumoModule.__init__(self, name, manager, configuration)
        self.murmur = manager.getMurmurModule()
        self.data = {}
        # The dice
        self.dice = 0
        # Nb of dices
        self.nb_dice = 0
        # The result modifier
        self.modifier = 0
        # The additioned dices
        self.result = 0
        # Result dice per dice
        self.results = []


    def connected(self): 
        manager = self.manager()
        log = self.log()
        log.debug("Register for Server callbacks")

        servers = self.cfg().dice.servers
        if not servers:
            servers = manager.SERVERS_ALL

        manager.subscribeServerCallbacks(self, servers)

    def disconnected(self): pass

    def roll_dice(self):
        return random.SystemRandom().randint(1, self.dice)

    def parse_command(self, command):
        cmd = command.replace(" ", "").replace("!rp", "").replace("!r", "")
        if "+" in cmd:
            self.modifier = int(cmd.split("+")[1])
            tmp = cmd.split("d")
            self.dice = int(tmp[1].split("+")[0])
            self.nb_dice = int(tmp[0])
        elif "-" in cmd:
            self.modifier = -int(cmd.split("-")[1])
            tmp = cmd.split("d")
            self.dice = int(tmp[1].split("-")[0])
            self.nb_dice = int(tmp[0])
        else:
            tmp = cmd.split("d")
            self.dice = int(tmp[1])
            self.nb_dice = int(tmp[0])
            self.modifier = 0

    def roll_result(self):
        self.results = []
        self.result = 0
        for dice in range(1, self.nb_dice + 1):
            tmp = self.roll_dice()
            self.result += tmp
            self.results.append(tmp)
        self.result += self.modifier
        
    def make_message(self, user):
        blacklist_os = ["","Browser", "Android", "iOS"]
        if user.os not in blacklist_os:
            msg = """
                <style>
                    .styled-table tbody tr.active-row {
                        font-weight: bold;
                        color: #00cca3;
                        background: #f3f3f3;
                    }
                    .styled-table tbody tr {
                        border-bottom: 1px solid #dddddd;
                        color: #00cca3;
                        background:#b3b3b3;
                    }

                    .styled-table tbody tr:nth-of-type(even) {
                        background-color: #f3f3f3;
                    }
                    .styled-table th,
                    .styled-table td {
                        padding: 2px 5px;
                    }
                    .styled-table thead tr {
                        background-color: #009879;
                        color: #ffffff;
                        text-align: center;
                    }
                    .styled-table {
                        border-collapse: collapse;
                        font-size: 0.9em;
                        font-family: sans-serif;
                    }

                    .styled-table tbody tr:last-of-type {
                        border-bottom: 2px solid #009879;
                    }
                    .crit{
                        font-weight:600; 
                    }
                    .good_crit{
                        color:green
                    }
                    .bad_crit{
                        color:red
                    }
                </style><br>"""
        else:
            msg = "<br>"
        val_occur = {}
        for dice in self.results:
            val_occur.setdefault(dice, 0)
            val_occur[dice] += 1
            # Good crit
            if dice == self.dice:
                msg = f"{msg}<span class='crit good_crit'>{dice}</span> + "
            # Bad crit
            elif dice == 1:
                msg = f"{msg}<span class='crit bad_crit'>{dice}</span> + "
            else:
                msg = f"{msg}{dice} + "
        msg = msg[:-3]
        if user.os not in blacklist_os:
            msg = msg + """<table class="styled-table">
                              <thead>
                                 <tr>
                                    <th>Face</th>
                                    <th>Occurence</th> 
                                 </tr>
                              </thead><tbody>"""
            val_occur = {k: val_occur[k] for k in sorted(val_occur)}
            pair = 0
            for val, occur in val_occur.items():
                if occur:
                    msg = msg + f"<tr class='{'active-row' if pair%2 == 0 else ''}'>"
                    pair += 1
                    msg = f"{msg}<th>{val}</th> "
                    msg = f"{msg}<th>{occur}</th> "
                    msg = msg + "</tr>"
            msg = msg + "</tbody></table>"
        msg = f"{msg}<br>Total : {self.result}"
        return msg

    def userTextMessage(self, server, user, message, current=None): 
        if message.text.startswith("!help"):
            server.sendMessageChannel(user.channel, False, """<br>You can roll dice !
                                                              <br>!r XdY+Z for rolling X dice Y plus Z
                                                              <br>!rp XdY+Z  for rolling X dice Y plus Z, the result is only displayed to you
  """ )
        if message.text[:3] in  ["!r ","!rp"]:
            self.parse_command(message.text)
            self.roll_result()
            if message.text[:3] ==  "!r ":
                channel = (user.channel)
                for user_id, user_content in server.getUsers().items():
                    if user_content.channel == channel:
                        msg = self.make_message(user_content)
                        server.sendMessage(user_content.session, msg)
            elif message.text[:3] == "!rp":
                msg = self.make_message(user)
                server.sendMessage(user.session, msg)

    def userConnected(self, server, state, context = None): pass
    def userDisconnected(self, server, state, context = None): pass
    def userStateChanged(self, server, state, context = None): pass
    def channelCreated(self, server, state, context = None): pass
    def channelRemoved(self, server, state, context = None): pass
    def channelStateChanged(self, server, state, context = None): pass

    def __getattr__(self, item):
        """This is a bypass method for getting rid of all the callbacks that are not handled by this module.

        :param item: item that is being queried for
        :return: an unused callable attribute that does not do anything
        """
        def unused_callback(*args, **kwargs):
            pass
        return unused_callback

